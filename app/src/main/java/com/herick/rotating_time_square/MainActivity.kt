package com.herick.rotating_time_square

import android.animation.ObjectAnimator
import android.graphics.Rect
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.widget.ImageView
import android.widget.TextView
import android.view.MotionEvent
import android.view.View.OnTouchListener
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser
import kotlinx.coroutines.*
import java.lang.StringBuilder
import java.net.URL
import java.text.SimpleDateFormat

class MainActivity : AppCompatActivity() {

    var mBoxView: TextView? = null
    var mDrawer: ImageView? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mBoxView = findViewById(R.id.box)
        mDrawer = findViewById(R.id.drawer)

        // SPIN BOX - 1 rotation per second
        var viewObjectAnimator = ObjectAnimator.ofFloat(mBoxView, "rotation", 0f, 360f)
        viewObjectAnimator.duration = 1000
        viewObjectAnimator.repeatCount = ObjectAnimator.INFINITE
        viewObjectAnimator.repeatMode = ObjectAnimator.RESTART
        viewObjectAnimator.start()

        // DRAG AND DROP
        var dx = 0.0f
        var dy = 0.0f
        var drawerRect = Rect()
        var boxRect = Rect()
        var didBoxIntersect = false

        var listener = OnTouchListener { view, motionEvent ->
            when (motionEvent.action) {
                MotionEvent.ACTION_DOWN -> {
                    dx = view.x - motionEvent.rawX
                    dy = view.y - motionEvent.rawY
                    mDrawer?.animate()
                        ?.scaleY(10f)
                        ?.setDuration(500)
                        ?.start()
                }

                MotionEvent.ACTION_MOVE -> {
                    view.animate()
                        .x(motionEvent.rawX + dx)
                        .y(motionEvent.rawY + dy)
                        .setDuration(0)
                        .start()
                }
                MotionEvent.ACTION_UP -> {
                    mDrawer?.getHitRect(drawerRect)
                    mBoxView?.getHitRect(boxRect)

                    // If box is at least 25% inside of the drawer
                    didBoxIntersect = percentageOverlap(boxRect, drawerRect) > 0.25

                    if (!didBoxIntersect) {
                        mDrawer?.animate()
                            ?.scaleY(1f)
                            ?.setDuration(500)
                            ?.start()
                    }
                }
            }

            return@OnTouchListener true
        }

        mBoxView?.setOnTouchListener(listener)

        // Update time
        val handler = Handler()
        handler.postDelayed(object : Runnable {
            override fun run() {
                GlobalScope.async {
                    updateTime()
                }
                handler.postDelayed(this, 1)
            }
        }, 1)
    }

    // percent that rect A is inside rect B
    private fun percentageOverlap(a: Rect, b: Rect) : Float {
        val overlap = ((Math.max(a.left, b.left) - Math.min(a.right,b.right)) * (Math.max(a.top, b.top) - Math.min(a.bottom, b.bottom))).toFloat()
        return overlap/(a.width() * a.height())
    }

    private suspend fun updateTime() {
        val time = GlobalScope.async {
            getCurrentTime()
        }

        withContext(Dispatchers.Main) {
            mBoxView?.text = time.await()
        }
    }

    private fun getCurrentTime(): String {
        val result = URL( "https://dateandtimeasjson.appspot.com").readText()
        val stringBuilder = StringBuilder(result)
        val json = Parser().parse(stringBuilder) as JsonObject
        val dateString = json.string("datetime")
        val dateTime = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSSSSS").parse(dateString)

        return SimpleDateFormat("HH:mm:ss").format(dateTime)
    }
}